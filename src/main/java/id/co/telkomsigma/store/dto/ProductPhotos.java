package id.co.telkomsigma.store.dto;

import lombok.Data;

@Data
public class ProductPhotos {
    private String id;
    private Product product;
    private String url;
}
