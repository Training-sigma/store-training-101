package id.co.telkomsigma.store.dto;

import lombok.Data;

@Data
public class CartItem {
    private Product product;
    private Integer jumlah;
}
